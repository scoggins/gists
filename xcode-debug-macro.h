/*
Author: Simon Coggins
Description: This is the maro I use to add debugging output to my apps. It is 
enabled when -DDEBUG is defined, but when it's not, all the debug code is removed.
*/

#if DEBUG
#include <libgen.h>
#define ZDebug(fmt, args...)	 NSLog(@"[%s:%d] %@\n", basename(__FILE__), __LINE__, [NSString stringWithFormat:fmt, ##args])
#else
#define ZDebug(fmt, args...)	 ((void)0)
#endif
