# A bitbucket of Gists

This is a place where I can store random snippets of code that I intend to link to via web and update from time to time.

* git-edit.sh	- A script to edit a file, and if git is in use, prompt to review/commit changes.
